package delfia.puja.day6

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.frag_data_prodi.view.*

class FragmentProdi : Fragment(), View.OnClickListener{
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btninsert -> {
                 builder.setTitle("konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                     .setIcon(android.R.drawable.ic_dialog_info)
                     .setPositiveButton("ya",btnInsertDialog)
                     .setNegativeButton("tidak",null)
                 builder.show()
            }
            R.id.btnhapus -> {
                builder.setTitle("konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("ya",btnDeleteProdi)
                    .setNegativeButton("tidak",null)
                builder.show()
            }
            R.id.btnupdate -> {
                builder.setTitle("konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("ya",btnUpdateDialog)
                    .setNegativeButton("tidak",null)
                builder.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idprodi : String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.frag_data_prodi,container,false)
        v.btnupdate.setOnClickListener(this)
        v.btninsert.setOnClickListener(this)
        v.btnhapus.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.isProdi.setOnItemClickListener(itemClick)

        return v
    }
    fun showDataProdi(){
        val cursor : Cursor = db.query("prodi", arrayOf("nama_prodi","id_prodi as _id"),
            null,null,null,null,"nama_prodi asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_prodi,cursor,
        arrayOf("_id","nama_prodi"), intArrayOf(R.id.txProdi, R.id.txNamaProdi),
        CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.isProdi.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataProdi()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idprodi = c.getString(c.getColumnIndex("_id"))
        v.edNamaProdi.setText(c.getString(c.getColumnIndex("nama_prodi")))
    }
    fun insertDataProdi(namaProdi : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_prodi",namaProdi)
        db.insert("prodi",null,cv)
        showDataProdi()
    }
    fun updateDataProdi(namaProdi: String, idProdi:String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_prodi",namaProdi)
        db.update("prodi",cv,"id_prodi = $idProdi",null)
        showDataProdi()
    }
    fun deleteDataProdi(isProdi : String){
        db.delete("prodi","id_prodi = $idprodi",null)
        showDataProdi()
    }
    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataProdi(v.edNamaProdi.text.toString())
        v.edNamaProdi.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataProdi(v.edNamaProdi.text.toString(),idprodi)
        v.edNamaProdi.setText("")
    }
    val btnDeleteProdi = DialogInterface.OnClickListener { dialog, which ->
        deleteDataProdi(idprodi)
        v.edNamaProdi.setText("")
    }
}