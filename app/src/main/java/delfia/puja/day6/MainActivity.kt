package delfia.puja.day6

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.frag_data_prodi.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener{

    lateinit var db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    //lateinit var fragMatkul : FragmentMatkul
    lateinit var fragKali : FragmentKali
    //lateinit var fragNilai : FragmentNilai
    lateinit var ft : FragmentTransaction

    val RC_PERKALIAN_SUKSES : Int = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragKali = FragmentKali()
       // fragMatkul = FragmentMatkul()
        //fragNilai = FragmentNilai()
        db = DBOpenHelper(this).writableDatabase
    }
    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.layout,fragProdi).commit()
                layout.setBackgroundColor(Color.argb(245,255,255,225))
                layout.visibility = View.VISIBLE
            }

            R.id.temMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.layout,fragMhs).commit()
                layout.setBackgroundColor(Color.argb(245,225,255,255))
                layout.visibility = View.VISIBLE
            }
            R.id.itemKali ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.layout,fragKali).commit()
                layout.setBackgroundColor(Color.argb(245,225,255,255))
                layout.visibility = View.VISIBLE
            }

            R.id.itemAbout -> layout.visibility = View.GONE
        }

        return true
    }
}
