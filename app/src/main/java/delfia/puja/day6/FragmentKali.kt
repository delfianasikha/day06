package delfia.puja.day6

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_kali.*
import kotlinx.android.synthetic.main.frag_data_kali.view.*
import java.text.DecimalFormat

class FragmentKali : Fragment(), View.OnClickListener {
    lateinit var thisParent: MainActivity
    lateinit var v: View

    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        x = edx.text.toString().toDouble()
        y = edy.text.toString().toDouble()
        hasil = x*y
        txhasil.text = DecimalFormat("#.##").format(hasil)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        //db = thisParent.getDBObject()
        v = inflater.inflate(R.layout.frag_data_kali, container, false)
        v.hasil.setOnClickListener(this)

        return v
    }
}